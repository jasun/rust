Alpine docker build container for rust

Build with:
   ```
   docker build --rm -t rust_build .   
   ```
Run with:
   ```
   docker run -it -v <local_rust_source_dir>:/<docker_dir> rust_build /bin/bash

   EXAMPLE:
   docker run -it -v ~/git/rust/powerball:/git rust_build /bin/bash
   ```
