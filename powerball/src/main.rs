use rand::Rng;

fn populate_whiteball_pool( pool: &mut std::vec::Vec<i8> ) {
    for x in 1..70 {
        pool.push(x)
    }
}

fn populate_powerball_pool( pool: &mut std::vec::Vec<i8> ) {
    for x in 1..27 {
        pool.push(x)
    }
}

fn get_powerball ( pool: std::vec::Vec<i8> ) -> i8 {
    let slot: usize = rand::thread_rng().gen_range( 0, pool.len() as usize ); 
    return pool[ slot ]; 
}

fn get_whiteball ( pool: &mut std::vec::Vec<i8> ) -> i8 {
    let slot: usize = rand::thread_rng().gen_range( 0, pool.len() as usize ); 
    let whiteball_number: i8 = pool[ slot ]; 
    pool.remove( slot );
    return whiteball_number; 
}

fn main() {
    #[derive(Debug)]
    struct PbNumber {
        whiteball: [i8; 5],
        powerball: i8
    }

    let mut whiteball_pool: Vec<i8> = Vec::with_capacity(69);
    let mut powerball_pool: Vec<i8> = Vec::with_capacity(69);

    populate_whiteball_pool( &mut whiteball_pool );
    populate_powerball_pool( &mut powerball_pool );

    let mut counter: usize = 0;

    loop {
        counter = counter + 1;

        // Struct to hold powerball numbers
        let mut _tmp_pb_number = PbNumber{
            whiteball: [0; 5],
            powerball: 0
        }; 

        // Clone populated ball pool to pull from
        // This way each loop uses a fresh pool to pull from
        let mut _tmp_whiteball_pool = whiteball_pool.clone();
        let mut _tmp_powerball_pool = powerball_pool.clone();

        // Get whiteballs        
        for i in 0..5 {
            _tmp_pb_number.whiteball[i] = get_whiteball( &mut _tmp_whiteball_pool );
        }

        // Get powerball
        _tmp_pb_number.powerball = get_powerball( _tmp_powerball_pool );

        _tmp_pb_number.whiteball.sort();

        println!( "{} - {:?}", counter, _tmp_pb_number );

        if counter >= 10 {
            break;
        }
    }
}
